import React, {useEffect, useState} from 'react';
import {ScrollView, View, TouchableOpacity, Text, Image} from 'react-native';
import axios from 'axios';

const HarryPotter = () => {
  const [listData, setListData] = useState([]);

  const fetchData = async () => {
    try {
      const response = await axios.get(
        'https://hp-api.onrender.com/api/characters',
      );
      setListData(response.data.filter((item, index) => index < 10));
    } catch (e) {
      console.error('error', e);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <ScrollView style={{padding: '2%'}}>
      <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
        {listData.map((item, index) => (
          <TouchableOpacity
            key={index}
            style={{
              color: 'black',
              textDecoration: 'none',
              backgroundColor: '#ccc',
              marginBottom: 5,
              borderRadius: 10,
              padding: 10,
              width: '48%',
              margin: '1%',
            }}>
            <Image
              source={{uri: item.image}}
              style={{width: '100%', height: 200}}
            />
            <View>
              <Text
                style={{
                  fontSize: 18,
                  marginTop: 5,
                  fontWeight: 'bold',
                  color: '#000',
                }}>
                {item.name}
              </Text>
            </View>
            <View>
              <Text style={{color: '#333'}}>
                {item.dateOfBirth === ''
                  ? item.yearOfBirth === ''
                    ? 'Unknown'
                    : item.yearOfBirth
                  : item.dateOfBirth}
              </Text>
            </View>
          </TouchableOpacity>
        ))}
      </View>
    </ScrollView>
  );
};

export default HarryPotter;
