import React, {useEffect, useState} from 'react';
import {SafeAreaView, StyleSheet, Text, View, StatusBar} from 'react-native';
import Button from '../components/Button';
import Gap from '../components/Gap';
import {dimens} from '../utils';

const Detail = ({navigation, route}) => {
  const {articleID} = route?.params;

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor={'white'} barStyle="dark-content" />
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <Gap bottom={dimens[20]} />
          <Text style={styles.welcome}>
            Isi dari param articleID: {JSON.stringify(articleID)}
          </Text>
          <Gap bottom={dimens[20]} />
          <Button title="Back" onPress={() => navigation.goBack()} />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Detail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  wrapper: {
    flex: 1,
    paddingHorizontal: dimens[20],
  },
  welcome: {
    fontSize: 22,
    color: '#000',
  },
});
