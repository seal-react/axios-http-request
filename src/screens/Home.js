import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  StatusBar,
} from 'react-native';
import {useFormik} from 'formik';
import * as Yup from 'yup';
import {useDispatch, useSelector} from 'react-redux';
import {CommonActions} from '@react-navigation/native';
import Input from '../components/Input';
import Button from '../components/Button';
import Gap from '../components/Gap';
import {dimens} from '../utils';
import {setUser} from '../store/actionCreators';

const URL =
  /^((https?|ftp):\/\/)?(www.)?(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;

const FormSchema = Yup.object().shape({
  url: Yup.string()
    .matches(URL, 'Enter a valid URL')
    .required('URL is required'),
});

const Home = ({navigation}) => {
  const user = useSelector(state => state.user);
  const dispatch = useDispatch();
  const [result, setResult] = useState('');
  const formik = useFormik({
    initialValues: {
      url: '',
    },
    validationSchema: FormSchema,
    onSubmit: values => submit(values),
  });

  const logout = async values => {
    dispatch(setUser(null));
    alert('Success logout');
    navigation.navigate('Login');
  };

  const submit = async values => {
    alert(JSON.stringify(values));
    alert('Success submit shorten link');
  };

  const copyToClipboard = () => {
    alert('Success copy to clipboard');
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor={'white'} barStyle="dark-content" />
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <Gap bottom={dimens[20]} />
          <Text style={styles.welcome}>Welcome, {user?.email || 'Guest'}</Text>
          <Gap bottom={dimens[20]} />
          <Input
            label="Long URL Link"
            placeholder="https://example.com"
            value={formik.values.email}
            onChangeText={text => formik.setFieldValue('email', text)}
            withoutShadow
            autoCapitalize="none"
            errorText={formik.errors.email}
          />
          <Button title="Submit" onPress={formik.handleSubmit} />
          <View style={styles.row}>
            <View>
              <Text style={styles.bold}>Result:</Text>
              <Text>Tidak ada hasil</Text>
            </View>
            <TouchableOpacity onPress={copyToClipboard}>
              <Text style={styles.bold}>Copy</Text>
            </TouchableOpacity>
          </View>
          <Gap bottom={dimens[60]} />
          <Button
            title="History Shorten Link"
            onPress={() => navigation.navigate('Detail', {articleID: 20})}
          />
          <Gap bottom={dimens[20]} />
          <Button title="Logout" onPress={logout} />
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  wrapper: {
    flex: 1,
    paddingHorizontal: dimens[20],
  },
  welcome: {
    fontSize: 22,
    color: '#000',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: dimens[20],
  },
  bold: {
    fontWeight: 'bold',
  },
});
