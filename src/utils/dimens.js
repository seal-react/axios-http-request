const dimens = {
  2: 2,
  4: 4,
  5: 5,
  6: 6,
  8: 8,
  10: 10,
  12: 12,
  14: 14,
  16: 16,
  18: 18,
  20: 20,
  22: 22,
  24: 24,
  26: 26,
  28: 28,
  30: 30,
  32: 32,
  34: 34,
  36: 36,
  40: 40,
  42: 42,
  46: 46,
  50: 50,
  55: 55,
  56: 56,
  60: 60,
  65: 65,
  70: 70,
  75: 75,
  80: 80,
  120: 120,
  //   add more if needed
  //   key same as value for easily read from snippet
};

export {dimens};
